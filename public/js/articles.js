var gopost;
var page_num;
var cur_section = "all";
window.onload = function(){
	var tags;
	var posts;
	const database = firebase.database();

	firebase.auth().onAuthStateChanged(function(user){
		var account = document.getElementById("account");
		if(user){
			var useremail = user.email;
			firebase.database().ref("/users").once("value").then(function(snapshot){
				var username;
				const users = snapshot.val();
				for(var key in users){
					if(useremail == users[key].email){
						username = users[key].username;
						break;
					}
				}
				account.innerHTML = "<button class='menu_btn float-right' id='logout'>Log out</button><button class='menu_btn float-right' id='username'>"+username+"</button>";
				document.getElementById("logout").addEventListener("click", function(){
					firebase.auth().signOut().then(function(){
						alert("logout success");
					}).catch(function(error){
						alert("logout failed: "+error.message);
					});
				});
			}).catch(function(error){
				console.log(error.message);
			});
		} else
			account.innerHTML = "<button class='menu_btn float-right' onclick=\"localStorage['prev_page']='index.html';document.location='signin.html';\">Log in</button>";
	});

	const page_anchor = document.getElementById("page_anchor");

	database.ref("/posts/post_num").once("value").then(function(snapshot){
		page_num = Math.ceil(snapshot.val()/10);
		var page = localStorage["cur_page"];
		for(var i=1; i<=page_num; i++){
			if(i != page)
				page_anchor.innerHTML += "<button class='pages' id='page_"+i+"'>"+i+"</button>";
			else
				page_anchor.innerHTML += "<button class='pages page_selected' id='page_"+i+"'>"+i+"</button>";
		}
	});

	page_anchor.addEventListener("click", function(e){
		if(e.target.id != "page_anchor"){
			const page = e.target.id.split("_")[1];
			localStorage["cur_page"] = page;
			if(cur_section == "all"){
				database.ref("/posts/"+localStorage["cur_page"]).once("value").then(function(snapshot){
					const posts = snapshot.val();
					var getallpost = new Promise(function(resolve, reject){
						var allpost = [];
						console.log(posts);
						for(key in posts){
							const post = posts[key];
							database.ref("/users/"+post.uid).once("value").then(function(snapshot){
								allpost.push("<div class='posts' id='"+post.pid+"' onclick='gopost(this.id)'><h3>"+snapshot.val().username+": <small>"+post.title+"</small></h3></div>");
							});
							setTimeout(function(){
								console.log(allpost.length);
								if(Object.keys(posts).length == allpost.length)
									resolve(allpost);
							}, 500);
						}
					});
					getallpost.then(function(posts){
						articles.innerHTML = "";
						page_anchor.innerHTML = "";
						for(key in posts){
							articles.innerHTML += posts[key];
						}
						for(var i=1; i<=page_num; i++){
							if(i != page)
								page_anchor.innerHTML += "<button class='pages' id='page_"+i+"'>"+i+"</button>";
							else
								page_anchor.innerHTML += "<button class='pages page_selected' id='page_"+i+"'>"+i+"</button>";
						}
					});
				});
			} else{
				database.ref(cur_section+"/"+localStorage["cur_page"]).once("value").then(function(snapshot){
					const posts = snapshot.val();
					articles.innerHTML = "";
					var getallpost = new Promise(function(resolve, reject){
						var allpost = [];
						console.log(posts);
						for(var key in posts){
							const post = posts[key];
							database.ref("/posts/"+post.page+"/"+key).once("value").then(function(snapshot){
								const post = snapshot.val();
								database.ref("/users/"+post.uid).once("value").then(function(snapshot){
									allpost.push("<div class='posts' id='"+post.pid+"' onclick='gopost(this.id)'><h3>"+snapshot.val().username+": <small>"+post.title+"</small></h3></div>");
								});
								setTimeout(function(){
									console.log(allpost.length);
									if(Object.keys(posts).length == allpost.length)
										resolve(allpost);
								}, 500);
							});
						}
					});
					getallpost.then(function(posts){
						articles.innerHTML = "";
						page_anchor.innerHTML = "";
						for(key in posts)
							articles.innerHTML += posts[key];
						for(var i=1; i<=page_num; i++){
							if(i != page)
								page_anchor.innerHTML += "<button class='pages' id='page_"+i+"'>"+i+"</button>";
							else
								page_anchor.innerHTML += "<button class='pages page_selected' id='page_"+i+"'>"+i+"</button>";
						}
					});
				});
			}
		}
	});

	const sections = document.getElementById("sections");

	database.ref("/tags").once("value").then(function(snapshot){
		tags = snapshot.val();
		for(key in tags){
			const tagname = key;
			sections.innerHTML += "<button class='sections' id='"+tagname+"'>"+tagname+"</button>";
		}
		database.ref("/tags").on("value", function(new_snap){
			tags = new_snap.val();
			sections.innerHTML = "<button class='sections' id='all'>All</button>";
			for(key in tags){
				const tagname = key;
				sections.innerHTML += "<button class='sections' id='"+tagname+"'>"+tagname+"</button>";
			}
		});
	});

	const articles = document.getElementById("articles");

	database.ref("/posts/"+localStorage["cur_page"]).once("value").then(function(snapshot){
		posts = snapshot.val();
		for(key in posts){
			const post = posts[key];
			database.ref("/users/"+post.uid).once("value").then(function(snapshot){
				articles.innerHTML += "<div class='posts' id='"+post.pid+"' onclick='gopost(this.id)'><h3>"+snapshot.val().username+": <small>"+post.title+"</small></h3></div>";
			});
		}
	});

	sections.addEventListener("click", function(e){
		const id = e.target.id;
		localStorage["cur_page"] = 1;
		if(id != "sections"){
			localStorage["cur_page"] = 1;
			if(id == "all"){
				database.ref("/posts/"+localStorage["cur_page"]).once("value").then(function(snapshot){
					posts = snapshot.val();
					var getallpost = new Promise(function(resolve, reject){
						var allpost = [];
						for(key in posts){
							const post = posts[key];
							database.ref("/users/"+post.uid).once("value").then(function(snapshot){
								allpost.push("<div class='posts' id='"+post.pid+"' onclick='gopost(this.id)'><h3>"+snapshot.val().username+": <small>"+post.title+"</small></h3></div>");
							});
							setTimeout(function(){
								if(Object.keys(posts).length == allpost.length)
									resolve(allpost);
							}, 300);
						}
					});
					getallpost.then(function(posts){
						articles.innerHTML = "";
						page_anchor.innerHTML = "";
						for(key in posts){
							articles.innerHTML += posts[key];
						}	
						for(var i=1; i<=page_num; i++){
							if(i != 1)
								page_anchor.innerHTML += "<button class='pages' id='page_"+i+"'>"+i+"</button>";
							else
								page_anchor.innerHTML += "<button class='pages page_selected' id='page_"+i+"'>"+i+"</button>";
						}
					});
				});
			} else{
				cur_section = "/taged_posts/"+id;
				database.ref(cur_section+"/"+localStorage["cur_page"]).once("value").then(function(snapshot){
					const posts = snapshot.val();
					articles.innerHTML = "";
					var getallpost = new Promise(function(resolve, reject){
						var allpost = [];
						for(var key in posts){
							const post = posts[key];
							database.ref("/posts/"+post.page+"/"+key).once("value").then(function(snapshot){
								const post = snapshot.val();
								database.ref("/users/"+post.uid).once("value").then(function(snapshot){
									allpost.push("<div class='posts' id='"+post.pid+"' onclick='gopost(this.id)'><h3>"+snapshot.val().username+": <small>"+post.title+"</small></h3></div>");
								});
								setTimeout(function(){
									if(Object.keys(posts).length == allpost.length)
										resolve(allpost);
								}, 300);
							});
						}
					});
					getallpost.then(function(posts){
						articles.innerHTML = "";
						page_anchor.innerHTML = "";
						for(key in posts){
							articles.innerHTML += posts[key];
						}
						for(var i=1; i<=page_num; i++){
							if(i != 1)
								page_anchor.innerHTML += "<button class='pages' id='page_"+i+"'>"+i+"</button>";
							else
								page_anchor.innerHTML += "<button class='pages page_selected' id='page_"+i+"'>"+i+"</button>";
						}
					});
				});
			}
		}
	});

	gopost = function(id){
		localStorage["cur_pid"] = id;
		document.location = "post.html";
	}

	document.getElementById("upload").addEventListener("click", function(){
		var currentUser = firebase.auth().currentUser;
		if(!currentUser){
			document.location = "signin.html";
		} else{
			document.location = "upload.html";
		}
	});
};
