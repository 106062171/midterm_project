window.onload = function(){
	const database = firebase.database();

	database.ref("/posts/"+localStorage["cur_page"]+"/"+localStorage["cur_pid"]).once("value").then(function(snapshot){
		const post = snapshot.val();
		const tags = document.getElementById("tags");
		for(key in post.tags){
			tags.innerHTML += "<span>"+post.tags[key]+"</span>";
		}
		database.ref("/users/"+post.uid).once("value").then(function(snapshot){
			document.getElementById("username").innerHTML = snapshot.val().username;
			document.getElementById("title").innerHTML = post.title;
			document.getElementById("body").innerHTML = post.body;
			const comments = post.comments;
			var getallcom = new Promise(function(resolve, reject){
				var allcomments = [];
				for(var key in comments){
					const comment = comments[key];
					database.ref("/users/"+comment.uid+"/username").once("value").then(function(snapshot){
						allcomments.push("<p><b>"+snapshot.val()+": </b>"+comment.body+"</p>");
					});
				}
				setTimeout(function(){
					if(Object.keys(allcomments).length == allcomments.length)
						resolve(allcomments);
				}, 300);
			});
			getallcom.then(function(allcomments){
				const comments = document.getElementById("comments");
				comments.innerHTML = "";
				for(key=allcomments.length-1; key>=0; key--)
					comments.innerHTML += allcomments[key];
				database.ref("/posts/"+localStorage["cur_page"]+"/"+localStorage["cur_pid"]+"/comments").on("value", function(snapshot){
					const comments = snapshot.val();
					var getallcom = new Promise(function(resolve, reject){
						var allcomments = [];
						for(var key in comments){
							const comment = comments[key];
							database.ref("/users/"+comment.uid+"/username").once("value").then(function(snapshot){
								allcomments.push("<p><b>"+snapshot.val()+": </b>"+comment.body+"</p>");
							});
						}
						setTimeout(function(){
							if(Object.keys(allcomments).length == allcomments.length)
								resolve(allcomments);
						}, 300);
					});
					getallcom.then(function(allcomments){
						const comments = document.getElementById("comments");
						comments.innerHTML = "";
						for(key=allcomments.length-1; key>=0; key--)
							comments.innerHTML += allcomments[key];
					});
				});
			});
		});
	});

	firebase.auth().onAuthStateChanged(function(user){
		var comment_area = document.getElementById("comment_area");
		if(user){
			comment_area.innerHTML = '<textarea id="comment" placeholder="leave your comments here..."></textarea><button style="float:right" id="submit">SUBMIT</button>'
			document.getElementById("submit").addEventListener("click", function(){
				const comment = document.getElementById("comment");
				if(comment.value == "")
					return;
				database.ref("/users/"+user.uid+"/username").once("value").then(function(snapshot){
					const newCommentKey = database.ref("/posts/"+localStorage["cur_page"]+"/"+localStorage["cur_pid"]+"/comments").push().key;
					const commentdata = {
						uid: user.uid,
						cid: newCommentKey,
						body: comment.value
					};
					console.log("/posts/"+localStorage["cur_page"]+"/"+localStorage["cur_pid"]+"/comments");
					console.log(commentdata);
					database.ref("/posts/"+localStorage["cur_page"]+"/"+localStorage["cur_pid"]+"/comments").push(commentdata).then(function(){
						comment.value = "";
					});
				});
			});
		}else{
			comment_area.innerHTML = '<textarea id="comment" placeholder="sign in to comment here..." disabled></textarea><button id="signin">SIGNIN</button>';
			document.getElementById("signin").addEventListener("click", function(){
				localStorage["prev_page"] = "post.html";
				document.location = "signin.html";
			});
		}
	});
}
