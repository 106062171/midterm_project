window.onload = function(){
	localStorage["cur_page"] = 1;
	firebase.auth().onAuthStateChanged(function(user){
		var account = document.getElementById("account");
		if(user){
			var useremail = user.email;
			firebase.database().ref("/users/"+user.uid+"/username").once("value").then(function(snapshot){
				const username = snapshot.val();
				account.innerHTML = "<button class='menu_btn float-right' id='logout'>Log out</button><button class='menu_btn float-right' id='username'>"+username+"</button>";
				document.getElementById("logout").addEventListener("click", function(){
					firebase.auth().signOut().then(function(){
						alert("logout success");
					}).catch(function(error){
						alert("logout failed: "+error.message);
					});
				});
			}).catch(function(error){
				console.log(error.message);
			});
		} else{
			account.innerHTML = "<button class='menu_btn float-right' onclick=\"localStorage['prev_page']='index.html';document.location='signin.html';\">Log in</button>";
		}
	});
};
