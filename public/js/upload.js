window.onload = function(){
	var database = firebase.database();

	var selected_tags = document.getElementById("selected_tags");
	var unselected_tags = document.getElementById("unselected_tags");
	var new_tag = document.getElementById("new_tag");

	var tags = {};
	var tags_selected = [];

	document.getElementById("create_tag").addEventListener("click", function(){
		new_tag.style = "display: inline-block";
	});

	database.ref("/tags").once("value").then(function(snapshot){
		if(snapshot.val())
			tags = snapshot.val();
		else
			tags = {};
		for(var key in tags){
			const tagname = key;
			selected_tags.innerHTML += "<button id='selected_"+tagname+"' style='display: none;'>"+tagname+"</button>";
			unselected_tags.innerHTML += "<button id='unselected_"+tagname+"' style='display: inline-block;'>"+tagname+"</button>";
		}
		database.ref("/tags").on("value", function(newSnap){
			if(snapshot.val())
				tags = snapshot.val();
			else
				tags = {};
			selected_tags.innerHTML = "";
			unselected_tags.innerHTML = "";
			for(var key in tags){
				const tagname = key;
				selected_tags.innerHTML += "<button id='selected_"+tagname+"' style='display: none;'>"+tagname+"</button>";
				unselected_tags.innerHTML += "<button id='unselected_"+tagname+"' style='display: inline-block;'>"+tagname+"</button>";
			}
		});
	}).then(function(){
		new_tag.addEventListener("keydown", function(e){
			if(e.keyCode == 13){
				const reg = "^[a-zA-z0-9]+$";
				var tagname = new_tag.value;
				if(tagname.match(reg)){
					if(tags_selected.indexOf(tagname) == -1){
						tags_selected.push(tagname);
						if(!tags || !tags.hasOwnProperty(tagname)){
							selected_tags.innerHTML += "<button id='selected_"+tagname+"' style='display: inline-block;'>"+tagname+"</button>";
							unselected_tags.innerHTML += "<button id='unselected_"+tagname+"' style='display: none;'>"+tagname+"</button>";
						} else{
							document.getElementById("unselected_"+tagname).style = "display: none;";
							document.getElementById("selected_"+tagname).style = "display: inline-block";
						}
					}
					new_tag.value = "";
					new_tag.style = "display: none";
				} else{
					alert("tag name must contain only digits and alphabets");
				}
			}
		});

		unselected_tags.addEventListener("click", function(e){
			var id = e.target.id;
			if(id != "unselected_tags"){
				const tagname = id.split("_")[1];
				tags_selected.push(tagname);
				document.getElementById(id).style = "display: none;";
				document.getElementById("selected_"+tagname).style = "display: inline-block";
			}
		});

		selected_tags.addEventListener("click", function(e){
			var id = e.target.id;
			if(id != "selected_tags"){
				const tagname = id.split("_")[1];
				tags_selected.splice(tags_selected.indexOf(tagname), 1);
				document.getElementById(id).style = "display: none";
				document.getElementById("unselected_"+tagname).style = "display: inline-block";
			}
		});
	
		document.getElementById("commit").addEventListener("click", function(){
			var title = document.getElementById("title").value;
			var body = document.getElementById("body").value;
			if(!title || !body){
				alert("Title and body can not be empty!");
			} else{
				title.replace(/</g, "&lt");
				title.replace(/>/g, "&gt");
				console.log(title);

				body.replace(/</g, "&lt");
				body.replace(/>/g, "&gt");
				console.log(body);

				const user = firebase.auth().currentUser;
				database.ref("/users/"+user.uid+"/username").once("value").then(function(snapshot){
					var username = snapshot.val();
					database.ref("/posts/post_num").once("value").then(function(snapshot){
						var post_num;
						if(snapshot.val())
							post_num = snapshot.val()+1;
						else
							post_num = 1;
						database.ref("/posts/post_num").set(post_num);
						var page = Math.ceil(post_num/10);
						database.ref("/user_posts/post_num").once("value").then(function(snapshot){
							var user_post_num;
							if(snapshot.val())
								user_post_num = snapshot.val()+1;
							else
								user_post_num = 1;
							database.ref("/user_posts/"+user.uid+"/post_num").set(user_post_num);
							var user_page = Math.ceil(user_post_num/10);
							
							var newPostKey = firebase.database().ref("/posts/"+page).push().key;
							var metapost = {
								page: page
							};
							var post = {
								uid: user.uid,
								pid: newPostKey,
								title: title,
								tags: tags_selected,
								body: body,
							}

							var updates = {};
							updates["/posts/"+page+"/"+newPostKey] = post;
							updates["/user_posts/"+user.uid+"/"+user_page+"/"+newPostKey] = metapost;

							for(var key in tags_selected){
								var tagname = tags_selected[key];
								console.log(tagname);
								if(tags && tags.hasOwnProperty(tagname))
									tags[tagname] += 1;
								else
									tags[tagname] = 1;
								updates["/taged_posts/"+tagname+"/"+Math.ceil(tags[tagname]/10)+"/"+newPostKey] = metapost;
							}
							if(tags){
								database.ref("/tags").set(tags);
							} else
								database.ref("/tags").set(tags_selected);
							console.log(updates);
							database.ref().update(updates).then(function(){
								alert("upload success");
								document.location = "articles.html";
							});
						});
					});
				});
			}
		});
	});
	document.getElementById("cancel").addEventListener("click", function(){
		document.location = "articles.html";
	});
};
