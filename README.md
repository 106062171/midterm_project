# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Siple Forum
* Key functions (add/delete)
    1. post
	2. comment
    
* Other functions (add/delete)
    1. add hashtag

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description

# 作品網址：[xxxx]

# Components Description : 
1. post : post a post with a title, a body, and some of hashtags
2. comment : comment a post
3. add hashtag : add hash tag on a post
...

# Other Functions Description(1~10%) : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
...

## Security Report (Optional)
